package main

import (
	"adventOfCode2021/pkg/util"
	"fmt"
	"strings"
)

func main() {
	lines := util.ReadLines("input/day8.txt")
	sum := 0
	for _, line := range lines {
		ctx := make(map[string]int)
		splits := strings.Split(line, " | ")
		before, after := splits[0], splits[1]
		digits := strings.Split(before, " ")
		var options []string
		for _, digit := range digits {
			theDigit := strings.TrimSpace(digit)
			ok := observe(&ctx, theDigit)
			if !ok {
				options = append(options, theDigit)
			}
		}
		deduce(options, &ctx)
		//fmt.Println(ctx)
		x := calculate(ctx, strings.Split(after, " "))
		//fmt.Println(x)
		sum += x
	}
	fmt.Println(sum)
}

func calculate(ctx map[string]int, input []string) int {
	output := 0
	for _, seg := range input {
		output = output*10 + display(ctx, seg)
		//fmt.Println(seg, output)
	}
	return output
}

func display(ctx map[string]int, segment string) int {
	for key, value := range ctx {
		if overlap(key, segment) == len(segment) && len(segment) == len(key) {
			return value
		}
	}
	panic("Unknown segment")
}

func observe(ctx *map[string]int, text string) bool {
	_, ok := (*ctx)[text]
	if ok {
		return true
	}
	switch len(text) {
	case 2:
		(*ctx)[text] = 1
		return true
	case 4:
		(*ctx)[text] = 4
		return true
	case 3:
		(*ctx)[text] = 7
		return true
	case 7:
		(*ctx)[text] = 8
		return true
	}
	return false
}

func deduce(options []string, ctx *map[string]int) {
	if len(*ctx) < 4 {
		panic("deduce")
	}
	digit(ctx, 1)
	digit(ctx, 7)
	// todo 0 2 3 5 6 9
	var last []string
	for _, option := range options {
		switch len(option) {
		case 6:
			if overlap(option, digit(ctx, 7)) == 2 {
				(*ctx)[option] = 6
				continue
			}
		case 5:
			if overlap(option, digit(ctx, 7)) == 3 {
				(*ctx)[option] = 3
				continue
			} else if overlap(option, digit(ctx, 4)) == 3 {
				(*ctx)[option] = 5
				continue
			} else if overlap(option, digit(ctx, 4)) == 2 {
				(*ctx)[option] = 2
				continue
			}
		}
		last = append(last, option)
	}
	// todo 0 9
	for _, option := range last {
		if overlap(option, digit(ctx, 5)) == 5 {
			(*ctx)[option] = 9
		} else {
			(*ctx)[option] = 0
		}
	}

	//a := onlyInRight(one, seven)[0]
	//fmt.Println("a", string(a))
	//fiveDigits := filterNumberOfDigits(options, 5)

}

func onlyInRight(left string, right string) string {
	var out string
	for _, x := range right {
		if !strings.Contains(left, string(x)) {
			out += string(x)
		}
	}
	return out
}

func overlap(left string, right string) int {
	var out int
	for _, x := range left {
		if strings.Contains(right, string(x)) {
			out++
		}
	}
	return out
}

func filterNumberOfDigits(digits []string, n int) []string {
	var out []string
	for _, seg := range digits {
		if len(seg) == n {
			out = append(out, seg)
		}
	}
	return out
}

func union(in []string) string {
	var set map[string]bool
	for _, x := range in {
		for _, char := range x {
			set[string(char)] = true
		}
	}
	var out string
	for key, _ := range set {
		out += key
	}
	return out
}

func digit(ctx *map[string]int, digit int) string {
	for key, value := range *ctx {
		if value == digit {
			return key
		}
	}
	panic("nil digit")
}
