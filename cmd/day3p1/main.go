package main

import (
	"adventOfCode2021/pkg/util"
	"fmt"
)

func main() {
	var lines = util.ReadLines("input/day3p1.txt")

	var width = len(lines[0])

	var zeros []int
	var ones []int
	for i := 0; i < width; i++ {
		zeros = append(zeros, 0)
		ones = append(ones, 0)
	}

	for _, line := range lines {
		for i, char := range line {
			if char == '0' {
				zeros[i] += 1
			} else if char == '1' {
				ones[i] += 1
			}
		}
	}

	var gamma = 0
	var epsilon = 0
	for i := 0; i < width; i++ {
		if ones[i] > zeros[i] {
			gamma = 1 + (gamma << 1)
			epsilon = 0 + (epsilon << 1)
		} else if zeros[i] > ones[i] {
			gamma = 0 + (gamma << 1)
			epsilon = 1 + (epsilon << 1)
		} else {
			panic("zeros == ones")
		}
	}

	fmt.Println(gamma * epsilon)
}
