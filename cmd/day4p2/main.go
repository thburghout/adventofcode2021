package main

import (
	"adventOfCode2021/pkg/util"
	"fmt"
	"strings"
)

type board struct {
	numbers [][]int
	draws   [][]bool
}

func main() {
	lines := util.ReadLines("input/day4p1.txt")
	draws := util.SplitCommasInt(lines[0])
	boards := getBoards(lines)

	var winningBoard board
	winningDraw := -1
	for _, number := range draws {
		//fmt.Printf("[%d] DRAW %d ============\n", i, number)
		var newBoards []board
		for _, b := range boards {
			b.draw(number)
			//b.print()
			if !b.wins() {
				newBoards = append(newBoards, b)
			} else if len(boards) == 1 {
				winningBoard = b
				winningDraw = number
			}
		}
		boards = newBoards
		if winningDraw > -1 {
			break
		}
	}

	fmt.Println(winningBoard.score(winningDraw))
}

func (b board) print() {
	for x := 0; x < len(b.numbers); x++ {
		for y := 0; y < len(b.numbers); y++ {
			if b.draws[x][y] {
				fmt.Printf(" ~%2d", b.numbers[x][y])
			} else {
				fmt.Printf(" %3d", b.numbers[x][y])
			}
		}
		fmt.Print("\n")
	}
	fmt.Print("\n")
}

func (b *board) draw(number int) {
	for x, row := range b.numbers {
		for y, field := range row {
			if field == number {
				b.draws[x][y] = true
			}
		}
	}
}

func (b board) wins() bool {
	return b.winsColumn() ||
		b.winsRow()
}

func (b board) score(calledNumber int) int {
	score := 0
	for x := 0; x < len(b.numbers); x++ {
		for y := 0; y < len(b.numbers); y++ {
			if !b.draws[x][y] {
				score += b.numbers[x][y]
			}
		}
	}
	return calledNumber * score
}

func (b board) winsRow() bool {
	for x := 0; x < len(b.numbers); x++ {
		wins := true
		for y := 0; y < len(b.numbers); y++ {
			wins = wins && b.draws[x][y]
		}
		if wins {
			return true
		}
	}
	return false
}

func (b board) winsColumn() bool {
	for y := 0; y < len(b.numbers); y++ {
		wins := true
		for x := 0; x < len(b.numbers); x++ {
			wins = wins && b.draws[x][y]
		}
		if wins {
			return true
		}
	}
	return false
}

//func (b board) winsDiag() bool {
//	winsA := true
//	winsB := true
//	for i := 0; i < len(b.numbers); i++ {
//		winsA = winsA && b.draws[i][i]
//		winsB = winsB && b.draws[i][len(b.numbers)-1-i]
//	}
//	return winsA || winsB
//}

func (b *board) initDraw() {
	for _, row := range b.numbers {
		var bools []bool
		for _, _ = range row {
			bools = append(bools, false)
		}
		b.draws = append(b.draws, bools)
	}
}

func getBoards(lines []string) []board {
	var boards []board
	var b board
	for _, line := range lines {
		if strings.Contains(line, ",") {
			continue
		} else if len(line) == 0 {
			if len(b.numbers) > 0 {
				b.initDraw()
				boards = append(boards, b)
			}
			b = board{}
			continue
		}
		b.numbers = append(b.numbers, util.SplitSpacesInt(line))
	}
	b.initDraw()
	boards = append(boards, b)
	return boards
}
