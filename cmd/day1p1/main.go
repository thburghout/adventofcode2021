package main

import (
	"adventOfCode2021/pkg/util"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	raw_data, err := os.ReadFile("input/day1p1.txt")
	util.Check(err)
	data := string(raw_data)
	str_lines := strings.Split(data, "\n")
	var input []int
	for _, x := range str_lines {
		if len(x) == 0 {
			continue
		}
		i, err := strconv.Atoi(x)
		util.Check(err)
		input = append(input, i)
	}

	//fmt.Println(input)

	output := 0

	for i := 1; i < len(input); i++ {
		if input[i] > input[i-1] {
			output += 1
		}
	}

	fmt.Println(output)
}

