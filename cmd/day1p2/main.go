package main

import (
	"adventOfCode2021/pkg/util"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	raw_data, err := os.ReadFile("input/day1p1.txt")
	util.Check(err)
	data := string(raw_data)
	str_lines := strings.Split(data, "\n")
	var input []int
	for _, x := range str_lines {
		if len(x) == 0 {
			continue
		}
		i, err := strconv.Atoi(x)
		util.Check(err)
		input = append(input, i)
	}

	//fmt.Println(input)

	var sums []int

	for i := 2; i < len(input); i++ {
		sums = append(sums, input[i] + input[i-1] + input[i-2])
	}

	var output int
	for i := 1; i < len(sums); i++ {
		if sums[i] > sums[i-1] {
			output++
		}
	}

	fmt.Println(output)
}

