package main

import (
	"adventOfCode2021/pkg/util"
	"fmt"
	"strings"
)

type command struct {
	action string
	units  int
}

func main() {
	var lines = util.ReadLines("input/day2p1.txt")
	var commands []command
	for _, line := range lines {
		var elems = strings.Split(line, " ")
		commands = append(commands, command{elems[0], util.StrToInt(elems[1])})
	}

	var x = 0
	var y = 0
	var aim = 0
	for _, cmd := range commands {
		switch cmd.action {
		case "forward":
			x += cmd.units
			y += aim * cmd.units
		case "down":
			aim += cmd.units
		case "up":
			aim -= cmd.units
		}
	}

	fmt.Println(x * y)
}
