package main

import (
	"adventOfCode2021/pkg/util"
	"fmt"
)

const GridSize = 1000

type vec struct {
	x1 int
	y1 int
	x2 int
	y2 int
}

type point struct {
	x int
	y int
}

func main() {
	lines := util.ReadLines("input/day5.txt")
	vecs := getVecs(lines)
	grid := make([][]int, GridSize)
	for i := range grid {
		grid[i] = make([]int, GridSize)
	}

	for _, v := range vecs {
		plot(&grid, v)
	}

	printGrid(grid)
	fmt.Println(danger(grid))
}

func danger(grid [][]int) int {
	x := 0
	for _, row := range grid {
		for _, v := range row {
			if v >= 2 {
				x += 1
			}
		}
	}
	return x
}

func plot(grid *[][]int, v vec) {
	for _, p := range points(v) {
		(*grid)[p.y][p.x] += 1
	}
}

func points(v vec) []point {
	xDelta := v.x2 - v.x1
	yDelta := v.y2 - v.y1
	var points []point
	//fmt.Println("== Points for ", v)
	if xDelta != 0 && yDelta != 0 {
		for x, y := 0, 0; x != (xDelta+inc(xDelta)) && y != (yDelta+inc(yDelta)); x, y = x+inc(xDelta), y+inc(yDelta) {
			p := point{v.x1 + x, v.y1 + y}
			//fmt.Println(p)
			points = append(points, p)
		}
		return points
	}
	for x := 0; x != (xDelta + inc(xDelta)); x += inc(xDelta) {
		for y := 0; y != (yDelta + inc(yDelta)); y += inc(yDelta) {
			p := point{v.x1 + x, v.y1 + y}
			//fmt.Println(p)
			points = append(points, p)
		}
	}
	return points
}

func inc(v int) int {
	if v >= 0 {
		return 1
	} else if v < 0 {
		return -1
	}
	panic("inc")
}

func getVecs(lines []string) []vec {
	var vecs []vec
	for _, line := range lines {
		var v vec
		_, err := fmt.Sscanf(line, "%d,%d -> %d,%d", &v.x1, &v.y1, &v.x2, &v.y2)
		util.Check(err)
		vecs = append(vecs, v)
	}
	return vecs
}

func printGrid(grid [][]int) {
	for _, row := range grid {
		for _, x := range row {
			if x == 0 {
				fmt.Print(".")
			} else {
				fmt.Print(x)
			}
		}
		fmt.Println()
	}
}
