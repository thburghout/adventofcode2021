package main

import (
	"adventOfCode2021/pkg/util"
	"fmt"
	"strconv"
)

func main() {
	var lines = util.ReadLines("input/day3p1.txt")

	var oxygen = findRating(lines, '1', '0', '1')
	var scrubber = findRating(lines, '0', '1', '0')

	fmt.Println(oxygen * scrubber)
}

func findRating(lines []string, mostOnes byte, mostZeros byte, same byte) int64 {

	width := len(lines[0])

	for i := 0; i < width; i++ {
		zeros := 0
		ones := 0
		for _, line := range lines {
			if line[i] == '0' {
				zeros += 1
			} else if line[i] == '1' {
				ones += 1
			} else {
				panic("foo")
			}
		}
		var newLines []string
		var search byte
		if ones > zeros {
			search = mostOnes
		} else if zeros > ones {
			search = mostZeros
		} else {
			search = same
		}
		for _, line := range lines {
			if line[i] == search {
				newLines = append(newLines, line)
			}
		}
		if len(newLines) == 1 {
			found := newLines[0]
			i, err := strconv.ParseInt(found, 2, 64)
			util.Check(err)
			return i
		}
		lines = newLines
	}
	panic("no more lines")
}
