package main

import (
	"adventOfCode2021/pkg/util"
	"fmt"
)

const MaxFishAge = 9

type State struct {
	fish []int
}

func main() {
	lines := util.ReadLines("input/day6p1.txt")
	fish := util.SplitCommasInt(lines[0])
	state := new(fish)

	//fmt.Println(state)
	for i := 0; i < 256; i++ {
		state.tick()
		//fmt.Println(state)
	}

	fmt.Println(state.count())
}

func new(initFish []int) State {
	var state State
	state.fish = make([]int, MaxFishAge)
	for _, fish := range initFish {
		state.fish[fish] += 1
	}
	return state
}

func (state *State) tick() {
	newFish := make([]int, MaxFishAge)

	for i, fish := range state.fish {
		if i == 0 {
			newFish[6] = fish
			newFish[8] = fish
		} else {
			newFish[i-1] += fish
		}
	}
	copy(state.fish, newFish)
}

func (state State) count() int {
	sum := 0
	for _, fish := range state.fish {
		sum += fish
	}
	return sum
}
