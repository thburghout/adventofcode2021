package main

import (
	"adventOfCode2021/pkg/util"
	"fmt"
	"strings"
)

type command struct {
	action   string
	distance int
}

func main() {
	var lines = util.ReadLines("input/day2p1.txt")
	var commands []command
	for _, line := range lines {
		var elems = strings.Split(line, " ")
		commands = append(commands, command{elems[0], util.StrToInt(elems[1])})
	}

	var x = 0
	var y = 0
	for _, cmd := range commands {
		switch cmd.action {
		case "forward":
			x += cmd.distance
		case "down":
			y += cmd.distance
		case "up":
			y -= cmd.distance
		}
	}

	fmt.Println(x * y)
}
