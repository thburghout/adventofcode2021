package main

import (
	"adventOfCode2021/pkg/util"
	"fmt"
	"math"
)

func main() {
	xs := util.SplitCommasInt(util.ReadLines("input/day7.txt")[0])
	max := util.Max(xs)
	fmt.Println(xs)
	var ys [][]int
	for _, x := range xs {
		var zs []int
		for i := 0; i < max; i++ {
			zs = append(zs, triangle(int(math.Abs(float64(i-x)))))
		}
		ys = append(ys, zs)
	}
	ys = util.Transpose(ys)
	var us []int
	for _, y := range ys {
		us = append(us, util.Sum(y))
	}
	min, index := util.MinIndex(us)

	fmt.Println(min, index)
}

func triangle(n int) int {
	c := 0
	for i := n; i > 0; i-- {
		c += i
	}
	return c
}
