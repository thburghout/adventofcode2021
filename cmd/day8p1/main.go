package main

import (
	"adventOfCode2021/pkg/util"
	"fmt"
	"strings"
)

func main() {
	lines := util.ReadLines("input/day8.txt")
	arr := make([]int, 10)
	for _, line := range lines {
		splits := strings.Split(line, "|")
		_, after := splits[0], splits[1]
		digits := strings.Split(after, " ")
		for _, digit := range digits {
			x := getDigit(strings.TrimSpace(digit))
			if x != -1 {
				arr[x] += 1
			}
		}
	}
	fmt.Println(arr, util.Sum(arr))
}

func getDigit(text string) int {
	switch len(text) {
	case 2:
		return 1
	case 4:
		return 4
	case 3:
		return 7
	case 7:
		return 8
	default:
		return -1
	}
}
