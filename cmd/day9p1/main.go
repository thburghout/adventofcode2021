package main

import (
	"adventOfCode2021/pkg/util"
	"fmt"
)

type Point struct {
	x int
	y int
}

func main() {
	lines := util.ReadLines("input/day9.txt")
	var xs [][]int
	for _, line := range lines {
		var ys []int
		for _, char := range line {
			y := util.StrToInt(string(char))
			ys = append(ys, y)
		}
		xs = append(xs, ys)
	}
	points := lowPoints(xs)
	var risk_level int
	for _, p := range points {
		v := xs[p.x][p.y]
		risk_level += 1 + v
	}
	fmt.Println(risk_level)
}

func lowPoints(xs [][]int) []Point {
	var out []Point
	for x := 0; x < len(xs); x++ {
		for y := 0; y < len(xs[0]); y++ {
			p := Point{
				x: x,
				y: y,
			}
			ys := neighbours(xs, p)
			if util.Min(ys) > xs[x][y] {
				out = append(out, p)
			}
		}
	}
	return out
}

func neighbours(xs [][]int, point Point) []int {
	var out []int
	for _, p := range neighbourPoints(xs, point) {
		out = append(out, xs[p.x][p.y])
	}
	return out
}

func neighbourPoints(xs [][]int, point Point) []Point {
	var out []Point
	if point.x != 0 {
		out = append(out, Point{
			x: point.x - 1,
			y: point.y,
		})
	}
	if point.y != 0 {
		out = append(out, Point{
			x: point.x,
			y: point.y - 1,
		})
	}
	if point.x != (len(xs) - 1) {
		out = append(out, Point{
			x: point.x + 1,
			y: point.y,
		})
	}
	if point.y != (len(xs[0]) - 1) {
		out = append(out, Point{
			x: point.x,
			y: point.y + 1,
		})
	}
	return out
}
