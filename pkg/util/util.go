package util

import (
	"errors"
	"os"
	"strconv"
	"strings"
)

func Check(e error) {
	if e != nil {
		panic(e)
	}
}

func ReadLines(fileName string) []string {
	raw_data, err := os.ReadFile(fileName)
	Check(err)
	data := string(raw_data)
	str_lines := strings.Split(data, "\n")
	for {
		if len(str_lines[len(str_lines)-1]) == 0 {
			str_lines = str_lines[:len(str_lines)-1]
		} else {
			break
		}
	}
	return str_lines
}

func StrToInt(text string) int {
	i, err := strconv.Atoi(text)
	Check(err)
	return i
}

func SplitIntoInt(input string, sep string) []int {
	parts := strings.Split(input, sep)
	var numbers []int
	for _, p := range parts {
		if len(p) > 0 {
			numbers = append(numbers, StrToInt(p))
		}
	}
	return numbers
}

func SplitCommasInt(input string) []int {
	return SplitIntoInt(input, ",")
}

func SplitSpacesInt(input string) []int {
	return SplitIntoInt(input, " ")
}

func min(values []int) (min int, index int, e error) {
	if len(values) == 0 {
		return 0, 0, errors.New("Cannot detect a minimum value in an empty slice")
	}

	min = values[0]
	index = 0
	for i, v := range values {
		if v < min {
			min = v
			index = i
		}
	}

	return min, index, nil
}

func Min(values []int) int {
	v, _, e := min(values)
	Check(e)
	return v
}

func MinIndex(values []int) (int, int) {
	v, i, e := min(values)
	Check(e)
	return v, i
}

func max(values []int) (max int, e error) {
	if len(values) == 0 {
		return 0, errors.New("Cannot detect a minimum value in an empty slice")
	}

	max = values[0]
	for _, v := range values {
		if v > max {
			max = v
		}
	}

	return max, nil
}

func Max(values []int) int {
	v, e := max(values)
	Check(e)
	return v
}

func Transpose(slice [][]int) [][]int {
	xl := len(slice[0])
	yl := len(slice)
	result := make([][]int, xl)
	for i := range result {
		result[i] = make([]int, yl)
	}
	for i := 0; i < xl; i++ {
		for j := 0; j < yl; j++ {
			result[i][j] = slice[j][i]
		}
	}
	return result
}

func Sum(slice []int) int {
	s := 0
	for _, x := range slice {
		s += x
	}
	return s
}

func Product(slice []int) int {
	s := 1
	for _, x := range slice {
		s *= x
	}
	return s
}
